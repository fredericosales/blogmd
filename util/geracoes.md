<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## Gerações
---

[| back |](http://netlab.ice.ufjf.br/~frederico/)

#### Comportamentos geracionais: como as gerações se diferenciam
Em primeiro lugar, precisamos fazer um recorte temporal para que você entenda de
quem estamos falando.

Cada geração se refere aos nascidos em determinado intervalo de anos. Para o
marketing atual, nos referimos aos nascidos em meados do século XX até o início
do século XXI:

<center>
|    geração     | nacismento  |  idade  | observaçoes |
|:--------------:|:-----------:|:-------:|:-----------:|
|  Baby boomers  | 1940 - 1960 | 60 - 80 | paz e amor  |
|       X        | 1960 - 1980 | 40 - 60 |  perdidos   |
|       Y        | 1980 - 1995 | 25 - 40 | millennials |
|       z        | 1995 - 2010 | 10 - 25 | nuttelinhas |
| \\( \alpha \\) | 2010 - xxxx | 0  - 10 | ??????????  |
</center>

É preciso fazer algumas considerações sobre essas divisões. Essas datas de
nascimento variam muito conforme as fontes e não há um consenso sobre elas. A
separação por anos também não é estanque: por exemplo, alguém que nasceu em
1990 pode ter um comportamento parecido de alguém nascido em 1980 em outro
lugar do país, em outra situação socioeconômica, em outro contexto.

Além disso, os nascidos no início de uma geração sofrem influência da geração
anterior, por isso alguns comportamentos se misturam. Por exemplo, quem nasceu
no início da década de 80 talvez tenha mais características da Geração X que
dos millennials.

Portanto, entenda que as datas de nascimento são aproximadas e que essas
separações geracionais são apenas tendências que identificam grande parte do
grupo, mas que não podem ser generalizadas para todos os nascidos nestes anos.

A seguir, vamos entender melhor as características de cada grupo,
especialmente em relação ao consumo.

#### Geração Baby Boomers (Paz e Amor)
Começamos pela __Geração Baby Boomers__, que engloba os nascidos entre 1940 e 1960.
Se você lembrar das aulas de história, vai saber que os Baby Boomers nasceram
no período __pós-Segunda Guerra Mundial__ — um momento de reconstrução nos países
envolvidos.

No Brasil, esses anos coincidem com o desenvolvimentismo, quando Juscelino
Kubitschek prometia “50 anos em 5” e a Tropicália movimentava o cenário
cultural brasileiro. O país vivia uma fase de otimismo!

É nesses contextos que nasceram e cresceram os Baby Boomers, que hoje têm entre
60 e 80 anos. Segundo um estudo do __Grupo Padrão__, trata-se de uma geração
idealista, combativa, disciplinada e com espírito coletivo, responsável por
iniciar as lutas por direitos civis e políticos.

É também a primeira geração a conquistar o “direito de ser jovem” e a liberdade
de botar o pé na estrada, ouvir __rock’n’roll__ e curtir grandes festivais de
música — o que inspira os jovens até hoje!

Para o mercado de consumo, é importante perceber que os Baby Boomers concentram
hoje grande parte da riqueza mundial e as principais tomadas de decisão ao
redor do mundo, tanto no comando de países quanto de grandes empresas.

Porém, é uma geração que não nasceu nem cresceu no mundo acelerado que vivemos
hoje. Por isso, costuma ser mais resistente às mudanças, já que prioriza a
estabilidade, especialmente na carreira.

#### Geração X (soltos na bagaceira - durante e pós Paz e Amor <<>> culpados pelos millennials)
Quem nasceu a partir dos anos 1960 até 1980 é chamado de Geração X e tem hoje
entre 40 e 60 anos. Essas pessoas ainda guardam muitas das características dos
Baby Boomers, como a busca pela estabilidade na carreira, a disciplina e o
respeito pela hierarquia. Mas eles também reforçam a ideia de liberdade de
serem e curtirem o que quiserem.

Porém, essa geração crescem com a Guerra Fria e, aqui no Brasil, com a ditadura
civil-militar. Portanto, o otimismo já não é mais o mesmo.

A Geração X é mais cética em relação a autoridades e governantes, perde um
pouco do senso coletivo e se torna mais individualista e competitiva. Isso é
incentivado pela ação do marketing e da publicidade, que se intensifica sobre
eles.

Não é por acaso que muitos abriram sua própria empresa e hoje representam
grande parte dos empresários, inclusive de startups. Segundo a Fiesp, 38% das
startups brasileiras são de pessoas com mais de 45 anos.

Além disso, essa é uma forte geração no que diz respeito ao consumo. Embora não
tenha toda a riqueza dos Baby Boomers, a Geração X tem um alto poder de consumo
e procura aproveitar sua condição econômica da maneira mais intensa possível.

#### Geração Y (Millennials)
Essa geração poderá dizer: nós fomos os últimos a conhecer o mundo sem
internet. Nascida entre 1980 e 1995 e atualmente com 25 a 40 anos, a Geração Y
(os millennials) nasceu com a informática e a globalização e, com esses
fenômenos, transformou o mundo. Esse grupo viu a internet nascer, o mundo se
tornar mais veloz e a informação circular rapidamente em questão de segundos.

No Brasil, os millennials nasceram no contexto da redemocratização (após a
ditadura civil-militar) e da instabilidade econômica e política, mas viram o
país começar a dar passos mais seguros com o surgimento do Plano Real.

Diante desses contextos, os millennials se tornaram muito mais flexíveis às
mudanças. Mais que isso: eles são ávidos pela inovação e pelos desafios das
transformações. Para eles, não importa tanto a estabilidade, e sim a paixão, a
ousadia, a experiência.

Os millennials também são questionadores. Por terem crescido com a
globalização, eles desenvolveram uma visão global — mas não querem mais o mundo
que os pais e avós deixaram. Eles priorizam a sustentabilidade porque se
preocupam com o futuro do planeta, defendem o consumo consciente e gostam de se
engajar em causas sociais.

Como estão constantemente conectados, constroem e participam de redes de
relacionamento que corroboram os seus valores e ideais. Sua identidade já não
se limita mais ao lugar onde vivem e sua mensagem alcança muito mais que um
círculo de amigos — a internet transformou os millennials em cidadãos globais.

Porém, eles também são caracterizados como imediatistas. Já que viram o mundo
acelerar, eles querem ter o que desejam o mais rápido possível — seja o sucesso
na carreira, seja uma mensagem no celular. Com isso, também aumentam os níveis
de medo e ansiedade dessa geração, que enfrenta sérios problemas psicológicos.

Além disso, os millennials desenvolveram um modo de pensar complexo, muitas
vezes fragmentado, que reflete o meio que eles mais usam para se relacionar e
se expressar: a internet. Isso resulta também em uma identidade fragmentada —
ora é uma coisa, ora é outra, ora é tudo ao mesmo tempo! —, mas nada fora do
normal para essa geração tão dinâmica.

Os millennials viraram recentemente alvo de estudos e análises por serem a
grande força no mercado de trabalho e de consumo. Afinal, representam a maior
parte da população economicamente ativa do país.

Um dos estudos mais conhecidos sobre a Geração Y foi realizado pela Box 1824 e
gerou este vídeo abaixo, que também ajuda a entender as gerações anteriores:

O estudo mostra que, com a chegada dos millennials ao mercado, a vontade de ser
jovem se tornou uma obsessão

A Geração Y se tornou uma referência para os mais jovens e uma inspiração para
os mais velhos. Portanto, eles têm um alto poder de influência no consumo.

Outro estudo bastante interessante foi realizado pelo Think With Google e traz
uma divisão desse grupo: os Old Millennials e os Young Millennials.

#### Geração Z (filhos dos millennials)
Embora os millennials tenham ganhado bastante atenção nos últimos anos, é a
Geração Z (ou apenas GenZ) que está motivando mais estudos pelo marketing
atualmente. Você pode não ter percebido, mas a Geração Y já está envelhecendo,
e o marketing precisa conhecer melhor os jovens que estão chegando ao mercado e
ditando como as marcas devem se comportar.

Nascida entre 1995 e 2010, atualmente com 10 a 25 anos, a Geração Z já nasceu
em um mundo conectado e cresceu com um celular na mão — por isso também são
chamados de “nativos digitais”. Para eles, não existe divisão entre online e
offline, já que estão conectados a todo momento, em todo lugar.

Para a GenZ, não há tempo a perder. Eles são extremamente ágeis, multitarefas e
capazes de absorver uma grande quantidade de informações — afinal, vivem na era
do big data, da explosão de dados e precisam saber como lidar com eles.

Se a Geração Y já se preocupa com as questões ambientais e sociais, a Geração Z
vai além e transforma a preocupação em ativismo.

Segundo uma pesquisa do Think With Google, 85% dos jovens da Geração Z disseram
estar dispostos a doar parte do seu tempo para alguma causa. E um estudo da Box
1824 mostrou que 63% da Geração Z defende toda causa ligada à identidade das
pessoas (gênero, etnia e orientação sexual, por exemplo).

Pela internet, então, eles podem se manifestar livremente e expor suas opiniões
sobre temas importantes, seja por meio do “textão”, seja por meio de imagens ou
tweets curtos e diretos. As minorias, em especial, tornam-se temas centrais em
movimentos contra homofobia, racismo, machismo, xenofobia, entre outros.

Assim, eles conseguem angariar seguidores que compartilham dos mesmos
pensamentos, criar redes de ativismo e ainda mobilizar movimentos que saem das
telas e ocupam as ruas.

No Brasil, essa geração nasce em um momento de prosperidade, de crescimento
econômico e de busca por justiça social. Porém, na sua adolescência, já passa
pela crise política e econômica após as eleições presidenciais de 2014. São
esses adolescentes que se engajam em movimentos de contestação ao governo —
seja para um lado, seja para o outro — e se engajam politicamente.

Por isso, eles desenvolvem um forte senso crítico, que se torna marcante na sua
identidade. É com essa criticidade que eles enxergam a crise no Brasil,
enfrentam a recessão econômica e procuram respostas para melhorar a situação do
país, sem fugir da sua responsabilidade.

Ainda assim, a insegurança com o futuro é uma marca dessa geração. Por isso,
eles são mais pragmáticos e realistas que a geração anterior. Eles se preocupam
com o dinheiro e entendem que, mesmo que não tenham um emprego dos sonhos, a
carteira assinada é um caminho para a estabilidade financeira.

É de maneira crítica também que eles olham para o poder da internet e das redes
sociais. Embora sejam ferramentas poderosas para a militância e a mobilização,
elas também podem ser traiçoeiras ao promover um estilo de vida ilusório e
afetar a saúde mental, o que gera muitos casos de ansiedade, depressão e até
suicídio.

Além disso, essa geração tem uma identidade bastante fluida. Não tente
defini-los ou colocá-los em caixinhas — eles são o que quiserem ser, naquele
momento, naquele contexto. Eles são ainda mais plurais e dinâmicos que a
Geração Y e, por isso, diversidade e inclusão são conceitos intrínsecos à sua
identidade e à sua concepção de sociedade.

#### Geração \\( \alpha \\) (netos dos millennials)
Se a Geração Z é a bola da vez, a Geração Alpha é a primeira da fila na mira do
marketing. Nascidos em 2010, os integrantes dessa geração têm aproximadamente
10 anos hoje e ainda não entraram no mercado de consumo. Aliás, eles nem devem
ser alvos do marketing para crianças, que sofre restrições no mundo inteiro.

Mas isso não quer dizer que o marketing deve ignorar esse público que está
prestes a entrar na sua mira. Afinal, as reuniões de trabalho já estão
abordando o planejamento para os próximos anos. E as empresas devem conhecer
quem serão os consumidores no futuro próximo.

Porém, ainda não existem muitos estudos sobre a Geração Alpha. Sabe-se que eles
já nascem em um período de recessão econômica no Brasil e crescem em uma época
de polaridade e extremismo. Mas não há como prever o futuro e o efeito que isso
terá no seu comportamento.

Quanto às mídias, as crianças da Geração Alpha se relacionam naturalmente com o
celular e a internet. Porém, o que vai marcar essa geração é a sua relação com
a inteligência artificial. Dessa maneira, a tecnologia se torna ainda mais
integrada à sua vida, até mesmo ao seu próprio corpo.

Em entrevista ao Grupo Padrão, Fernanda Furia, mestre em psicologia de crianças
e adolescentes, afirma que a Geração Alpha será protagonista do início de uma
relação afetiva entre seres humanos e máquinas.

Essas crianças recebem tantos estímulos, mas lidam com isso desde tão cedo, que
provavelmente estarão mais preparadas para as transformações que estão por vir
— ou que elas mesmas vão provocar. Afinal, o poder de militância, contestação e
mobilização da geração anterior tende a ter continuidade.

Criadas pela Geração Y ou até pela Z, as crianças da Geração Alpha tendem a ser
ainda mais livres em relação à sua identidade. Meninas já não crescem mais em
um mundo cor-de-rosa, o que tende a torná-las cada vez mais protagonistas, em
posições de poder. Gênero e orientação sexual provavelmente não serão amarras,
assim como o direito à diferença será uma causa ainda mais fortalecida.

Mas, por enquanto, ainda estamos na base das suposições. Somente daqui a alguns
anos saberemos como essas crianças vão se comportar como jovens cidadãos e
consumidores.

### Aqui o mundo acaba...
Diferentes gerações usam diferentes linguagens, canais e conteúdos. Por isso,
por mais que os “jovens” pareçam um grupo único, é preciso usar uma comunicação
específica para cada geração em que eles estão divididos: as gerações
millennials (Y), Z e Alpha.

A partir da Geração Y, já se pode perceber uma preocupação dos jovens com a
sustentabilidade, a diversidade e o engajamento. Portanto, as marcas com que
eles se relacionam também devem se preocupar com isso. A ética das empresas é
um fator importante para a tomada de decisão nas relações de consumo.

Além disso, essa é a geração que viu a internet nascer: na sua infância, já
passou a usar smartphones. Portanto, a cultura digital e mobile faz parte da
sua vida.

Para muitas empresas, porém, essa era ainda não chegou. Não são poucas as
marcas que ainda não têm um site responsivo, por exemplo. Menos ainda são as
empresas que produzem conteúdos focados no consumo móvel, como a produção de
vídeos mobile. Não é dessa maneira, portanto, que elas vão conseguir se
conectar aos millennials.

Além de se adaptar às tecnologias que eles usam, o marketing também precisa
entender quais tipos de conteúdos eles gostam de consumir. É certo que a
publicidade tradicional quase não tem mais espaço, já que ela é interruptiva e
muitas vezes irrelevante.

O que a Geração Y quer é conteúdos de entretenimento, que aliviem a sua
ansiedade; conteúdos de ensino, que ajudem a aprender e vão direto ao ponto; ou
conteúdos de inspiração, que ajudem a realizar sonhos e projetos. Além disso,
eles querem se envolver com as marcas: não basta publicar um post nas redes
sociais, é preciso ouvir, entender, dar voz e interagir com os millennials para
conquistar sua confiança.

A GoPro é um exemplo de marca que consegue colocar os millennials no centro da
sua comunicação. Veja, por exemplo, como ela valoriza a interação com os
usuários ao publicar o conteúdo que eles produzem com seu produto na sua
própria página.

Se muitas empresas não conseguem conversar com os millennials porque não se
adaptaram às tecnologias e canais que eles usam, o que dizer da comunicação com
a Geração Z.

Essa geração nunca soube o que é a vida sem internet e smartphones! Se eles já
nasceram com um celular na mão, como vão se relacionar com marcas que pensam
apenas no desktop?

Por isso, o conceito de mobilidade deve estar arraigado nas estratégias das
empresas que querem se comunicar com a GenZ. Mais que isso, elas precisam
pensar na integração entre todas as mídias, sem que o usuário perceba quebras
na comunicação. Nesse sentido, o omnichannel é essencial para proporcionar a
melhor experiência.

Além disso, para falar com a Geração Z, não adianta fazer discursos vazios
sobre sustentabilidade ou responsabilidade social. Não adianta, por exemplo,
dizer que você é contra o racismo, mas não contratar funcionários negros ou não
tê-los em posições de liderança.

Essa geração quer ver compromissos reais, quer marcas que se posicionem,
especialmente a favor das minorias e dos direitos humanos, e que tenham
posições coerentes em todos os seus processos.

Para essa geração, também é importante pensar no poder dos influenciadores
digitais. A lógica dos ídolos da Geração Z não é a mesma das celebridades de
anos atrás. Eles precisam ser próximos dos fãs, falar a língua deles, ser
transparentes. Marcas que trabalham com influenciadores precisam entender isto:
autenticidade é palavra de ordem nas estratégias de marketing de influência.

Embora muitas marcas ainda não tenham percebido a importância de se posicionar
(ou ainda não tiveram coragem para isso), o que não faltam são exemplos de quem
já entendeu o recado da Geração Z. A Doritos, por exemplo, que busca se
aproximar dos jovens, lançou uma edição especial do produto cujo lucro foi
destinado a ONGs que apoiam a causa LGBT.

Se as marcas quiserem se comunicar com esse público no futuro, já precisam
compreender melhor a inteligência artificial, o machine learning e como essas
tecnologias podem ajudar a criar soluções cada vez mais eficientes, úteis e
personalizadas.

Como dissemos, as gerações millennials e Z costumam ser o foco do marketing,
mas os mais velhos não podem ficar de fora. Segundo o IBGE, o Brasil tem mais
de 30 milhões de idosos, e a tendência de envelhecimento da população continua.

As pessoas com 60 anos ou mais representam a geração mais rica da história —
afinal, teve que reconstruir países destroçados pelas guerras. Na carona dos
Baby Boomers, está a Geração X, entre 40 e 60 anos, que também concentra
riqueza e tem um alto poder de consumo.

Portanto, as marcas que buscam boas oportunidades precisam dar atenção à
comunicação com esses grupos, que estão bem vivos no mercado, buscando por
novas experiências e envelhecendo de maneira muito mais ativa que as gerações
anteriores.

Para concluir: a partir do final do século XX, o que vemos é uma aceleração da
história. O ritmo das mudanças se tornou frenético!

Com isso, o que fazia sentido dez anos atrás já não serve mais. E, com isso
também, cada vez se torna mais rápido o intervalo de uma geração para outra,
porque o mundo não muda mais a cada século — as transformações acontecem todo
ano, fazendo emergir novos padrões de comportamento.

Será que vamos chegar ao momento em que o intervalo de uma geração será de
apenas 10 anos, 5 anos ou até um ano? Não sabemos, mas é certo que o marketing
estará de olho nesse fenômeno para acompanhar as gerações que vêm por aí!

Atualmente, são as gerações millennials, Z e Alpha que estão ditando o mercado
e que o marketing precisa acompanhar. Mas em breve surge um novo grupo, pronto
para revolucionar a maneira de pensar das marcas!<br />
Author: [Camila Casarotto](https://rockcontent.com/br/blog/author/camila-casarotto/)

---
[| up |](http://netlab.ice.ufjf.br/~frederico/util/geracoes.md) [| back |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
<script>
	MathJax = {
		tex: {
			inlineMath: [['$', '$'], ['\\(', '\\)']]
		},

		svg: {
			fontCache: 'global'
		}
	};
</script>
