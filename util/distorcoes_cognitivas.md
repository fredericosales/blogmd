<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
### Distorsões cognitivas
---

[| back |](http://netlab.ice.ufjf.br/~frederico/)

As __DISTORÇÕES COGNITIVAS__ são basicamente maneiras distorcidas de processar uma
informação, ou seja, são interpretações enviesadas do que nos acontece, criando
diversas consequências negativas.  Quando se sofre de depressão, por exemplo,
tem-se uma visão da realidade na qual as distorções cognitivas exercem um papel
principal (visão de túnel). Todos nós podemos utilizar algum tipo de distorção
cognitiva. Saber detectá-las e analisá-las ajuda-nos a desenvolver atitudes
mais realistas e, acima de tudo, mais funcionais.

#### Personalização
Quando nos sentimos 100% responsáveis pelos acontecimentos. Por exemplo, o
filho do António fez um exame e não passou. O António pensa logo ter fracassado
na educação do seu filho, acha que cometeu algum erro, porque se tivesse feito
tudo bem feito, seu filho teria aprovado.

#### Filtro mental
Consiste em focarmo-nos nos aspectos negativos e ignorar o resto da informação.
O negativo é filtrado e absorvido, enquanto o positivo é esquecido. Exemplo: A
Maria preparou um prato para um jantar e convidou nove amigas.  Quase todas
adoram a refeição da Maria, à excepção da Cristina, que disse que o prato
estava um pouco salgado. Maria sente-se mal por isso e passa a achar que
cozinha terrivelmente mal -  Ela só absorveu o negativo, ignorando totalmente
os aspectos positivos.

Esta distorção também está presente quando acreditamos que se algo aconteceu
uma vez, acontecerá em todas as outras vezes. Por exemplo: O João terminou com
a Sónia depois de dois anos e meio de relação. A Sónia pensa “ninguém vai
gostar de mim”, “nunca mais encontrarei alguém que queira ficar comigo”.

#### Generalização
Essa distorção cognitiva consiste em maximizar os nossos próprios erros e os
acertos dos outros e, minimizar os próprios acertos e os erros dos outros. “Não
importa quantas coisas fiz correctamente no passado, elas não têm importância.
O que importa agora é que cometi um erro gravíssimo.”

#### Pesamento dicotómico
Consiste na extrema valorização dos acontecimentos, sem levar em conta os
aspectos intermediários. Classificar as coisas como brancas ou pretas,
verdadeiras ou falsas. Por exemplo: “Se este trabalho não ficar perfeito, o meu
esforço não terá valido para nada”, ou quando uma pessoa não encontra emprego e
pensa “sou completamente inútil!”.

#### Catastrofização
Ocorre quando prevemos o futuro negativamente sem considerar outros resultados
mais prováveis. Exemplo: “O meu filho ainda não chegou, deve ter acontecido
alguma coisa horrível. Vou esperar um pouco, mas não vou conseguir dormir.”
Outros exemplos: “O meu namorado não me atende o telemóvel, deve estar com
outra”.

#### Generalização
Ocorre quando generalizamos de um caso, para todos os casos, mesmo que seja
apenas ligeiramente idêntico. Se uma vez foi verdade, será sempre assim: “A mim
nada nunca corre bem”; “Nunca me vou casar”; “Eu nunca termino o que começo”;
“Eu jamais vou conseguir deixar de fumar”; “Não dormi bem ontem, a minha
insónia vai durar para sempre” ou “Nunca mais vou conseguir ter um emprego bom
como este”.

#### Raciocínio emocional
Refere-se à suposição de que as nossas emoções reflectem as coisas como elas
são. É acreditar que o que sentimos no momento é o correcto e verdadeiro.
“Estou-me a sentir uma incompetente, logo sou totalmente incompetente!” ou “Eu
sinto que é assim, consequentemente isso tem que ser verdade.”

#### Afirmações
São crenças rígidas e inflexíveis de como nós ou os demais deveríamos ser. As
exigências concentradas em nós próprios favorecem a autocrítica, enquanto as
dirigidas aos outros favorecem a raiva, a ira e a agressividade. Alguns
exemplos podem ser: “Deveria ter dado mais atenção ao meu marido, assim ele não
me teria deixado”, “Não devo cometer erros”, “Os outros devem-se portar bem
comigo” ou “Preciso gostar de todos.”

#### Leitura da mente
Consiste em afirmar que determinadas suposições são certas, mesmo que não
exista nenhuma evidência que a comprove. Acreditar que se sabe o que os outros
pensam e o motivo de se comportarem como se comportam. “O que ele quer é pôr-me
nervoso!”, “O que ele quer é rir-se de mim!”, “Eles sentem pena de mim!” ou
“Ela só está comigo por dinheiro!”

#### Predição do Futuro
Consiste em afirmar que determinadas suposições são certas, mesmo que não
exista nenhuma evidência para as comprovar, é esperar que nada dê certo, sem
sequer permitir a possibilidade de que seja razoável ou positiva. “Tenho
certeza de que vou reprovar.”,“Ninguém me vai dar atenção na festa”.

#### Rotulagem
Utilizar rótulos pejorativos para nos descrevermos, ao invés de descrever os
nossos actos e qualidades com objectividade e exactidão. Por exemplo: “Sou um
inútil!” ao invés de “Cometi um erro, mas nem sempre faço isso.”<br />
Author: [Rodrigues, Joana - acessado em 01/03/2021](http://claramente.pt)

#### Sobre a autora
Joana de São João Rodrigues é Psicóloga, Membro Efetivo da Ordem dos Psicólogos
e especialista em Psicologia Clínica e da Saúde. Possui licenciatura e mestrado
em Psicologia Clínica e da Saúde pela Faculdade de Psicologia da Universidade
de Lisboa. É Pós-Graduada em Educação Social e Intervenção Comunitária e Membro
Associado da Associação Portuguesa de Terapias Comportamental e Cognitiva. É
Formadora Certificada pelo Instituto do Emprego e Formação Profissional (IEFP)
e pelo Conselho Científico-Pedagógico da Formação Contínua. Desenvolveu
actividade clínica de apoio a doentes com doença oncológica e seus familiares
no Instituto Português de Oncologia Francisco Gentil do Centro Regional de
Oncologia de Lisboa, no Serviço da Clínica da Dor. Deu apoio psicológico às
famílias e crianças com cardiopatias congénitas internadas no Hospital Vall
d'Hebron para cirurgia através da Associació d'ajuda als Afectats de
Cardiopaties Infantils de Catalunya (Associação de ajuda aos Afectados por
Cardiopatias Infantis da Catalunha), em Barcelona. Desenvolveu actividade de
apoio às necessidades das mulheres vítimas de violência doméstica/abuso sexual,
pela Associação de Mulheres Contra a Violência, em Lisboa. Esteve integrada em
diversos projectos de Cooperação Internacional, onde deu formação na área da
promoção da saúde em Angola e Guiné-Bissau e foi técnica de cuidados
continuados integrados de saúde mental na Associação para o Estudo e Integração
Psicossocial, em Lisboa. Desde 2011 exerce clínica privada com jovens, adultos
e seniores e desde Março de 2016 que integra a Equipa da ClaraMente - Serviços
de Psicologia Clínica e Psicoterapia, com o objectivo de promover a saúde
mental e a qualidade de vida, disponibilizando serviços de Psicologia Clínica e
Psicoterapia em consultório em Lisboa e Caldas da Rainha. Desde Abril de 2019
que trabalha no Hospital Lusíadas em Lisboa, nas Unidades de Tratamento de Dor
e Oncologia.

---
[| up |](http://netlab.ice.ufjf.br/~frederico/util/distorcoes_cognitivas.md) [ back |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script>
    MathJax = {
        tex: {
            inlineMath: [[&lsquo;$&rsquo;, &lsquo;$&rsquo;], [&lsquo;\(&rsquo;, &lsquo;\)&rsquo;]]
        },

        svg: {
            fontCache: 'global'
        }
    };
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
