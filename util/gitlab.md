<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## gitlab
---

[| back |](http://netlab.ice.ufjf.br/~frederico/)

###### Commnad line instructions

You can also upload existing files from your computer using the instructions below.

###### git global setup

```bash

[user@machine ~/] $: git config --global user.name "John Doe"
[user@machine ~/] $: git config --global user.email "john.doe@some.address.br"
```

###### Create a new repository

```bash

[user@machine ~/] $: git clone git@gitlab.com:johndoe/project.git
[user@machine ~/] $: cd project
[user@machine ~/] $: git switch -c main
[user@machine ~/] $: touch README.md
[user@machine ~/] $: git add README.md
[user@machine ~/] $: git commit -m "add README"
[user@machine ~/] $: git push -u origin main
```

###### Push an existing folder

```bash

[user@machine ~/] $: cd existing_folder
[user@machine ~/] $: git init --initial-branch=master
[user@machine ~/] $: git remote add origin git@gitlab.com:johndoe/project.git
[user@machine ~/] $: git add .
[user@machine ~/] $: git commit -m "Initial commit"
[user@machine ~/] $: git push -u origin master
```

###### Push an existing Git repository

```bash

[user@machine ~/] $: cd existing_repo
[user@machine ~/] $: git remote rename origin old-origin
[user@machine ~/] $: git remote add origin git@gitlab.com:johndoe/project.git
[user@machine ~/] $: git push -u origin --all
[user@machine ~/] $: git push -u origin --tags
```

---
[| up |](http://netlab.ice.ufjf.br/~frederico/util/gitlab.md) [back |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
<script>
    MathJax = {
        tex: {
            inlineMath: [['$', '$'], ['\\(', '\\)']]
        },

        svg: {
            fontCache: 'global'
        }
    };
</script>
