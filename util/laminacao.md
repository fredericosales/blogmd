
<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## Laminação BOPP
---
[| home |](http://netlab.ice.ufjf.br/~frederico) [ back |](http://netlab.ice.ufjf.br/~frederico/bytes)

#### Bobina para [laminação](httuto.mercadolivre.com.br/MLB-1012229637-bobina-bopp-fosco-32-cm-x-200-mt-1-linha-_JM?quantity=1) a quente (110° e 120°)

As bobinas para laminação a quente  garantem maior durabilidade e resistência
ao seu impresso. Depois de aplicada oferecem ótima proteção à umidade, maior
espessura e corpo para sua mídia impressa. O Bopp é ideal para impressão
offset. Utilizando o Bopp fosco o seu impresso apresenta um acabamento sedoso,
sem reflexos, minimiza marcas de digitais.

#### Bobina bopp 

__Utilização:__

* Capas de livros;
* catálogos siacolas; 
* embalagens; 
* relatórios empresariais;
* folders;
* brochuras 
* cartões de visita;
* revistas;
* mapas;
* etiquetas;
* pontos de venda;
* mostruários.

***How to Laminate RC Models***

<p>
    <iframe width="560" height="315" src="https://www.youtube.com/embed/SYODnXIHVwE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</p>

***Temperatura de Aplicação:***

* 110° e 120º Compra 100%

***Especificações Técnicas:***

1. Tubo suporte 75mm de diamentro;
2. Espessura 24 micras;
3. Tamanho 320mm x 200m Fosco;

---
[| up | ](http://netlab.ice.ufjf.br/~frederico/util/laminacao) [back |](http://netlab.ice.ufjf.br/~frederico)
    
    All rights reserved to MadMonkeyBytes 2021.

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script>
    MathJax = {
        tex: {
            inlineMath: [[&lsquo;$&rsquo;, &lsquo;$&rsquo;], [&lsquo;\(&rsquo;, &lsquo;\)&rsquo;]]
        },</p>

    svg: {
        fontCache: 'global'
    }
};
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
