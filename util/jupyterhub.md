<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
#### JupyterHub server
---

[| back |](http://netlab.ice.ufjf.br/~frederico/)

#### How to implement a jupyterhub server.
Without further do let's do it. I will supose that docker and portairneir are installed and running.
If not remember, to be lazy some tools are necessary.

#### Starting the server
```bash
[user@hostname ~/ ]  $: docker run -d 8010:8000 --name jupyterhub satangos/jupyterhub jupyterhub

```
Testing the server. Go to you flavor browser and type http://172.17.0.X:8000, if you receive a login
prompt you don't mess up anything and move on (__remember the X__).

__X:__

```bash
[user@hostname ~/ ]  $: docker inspect -f  \
'{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' jupyterhub

```

And, SURPRISE: 172.17.0.X.

#### Enter in the matrix
With the server up now we need to create an user.

```bash
[user@hostname ~/ ] $: docker exec -it jupyterhub bash

```

Or open your flavor browser, login in portainer >> docker >> Containers >> jupyterhub >> >_Console >> connect.

####Creating a user:
```bash
[root@labhub ~/ ] $: adduser pequenoponey
Adding user `pequenoponey' ...
Adding new group `test' (1001) ...
Adding new user `test' (1001) with group `test' ...
Creating home directory `/home/pequenoponey' ...
Copying files from `/etc/skel' ...
New password: ********
Retype new password: ********
passwd: password updated successfully
Changing the user information for test
Enter the new value, or press ENTER for the default
        Full Name []: Pequeno Poney
        Room Number []: 666
        Work Phone []: 66 666-6666
        Home Phone []: 69 666-6666
        Other []: Pink Puk
Is the information correct? [Y/n]

```

####  Making this thing util


#### Go __Neo__, go...
```bash
[root@labhub ~/ ] $: su - pequenoponey

```

#### Create a config folder
```bash
[pequenoponey@labhub ~/] $: mkdir .jupyter

```

#### Create a config file
```bash
[pequenoponey@labhub ~/] $: touch .jupyter/jupyterhub_config.py

```

#### Make jupyterlab default notebook
```bash
[pequenoponey@labhub ~/] $: echo "c.Spawner.cmd=['jupyter-labhub']" \
> .jupyter/jupyterhub_config.py

```

#### Creating package requirements
```bash
[pequenoponey@labhub ~/] $: cat << EOF > requirements.txt
jupyterlab
plotly==4.14.3
notebook>=5.3
ipywidgets>=7.5
numpy
pandas
bokeh
scipy
statsmodels
scikit-learn
scikit-image
networkx
chart-studio
dash
jupyter-dash

EOF

```

#### Upgrading pip and setuptools
```bash
[pequenoponey@labhub ~/] $: pip install --upgrade pip setuptools

```

#### Installing python3 requirements
```bash
[pequenoponey@labhub ~/] $: pip install  -r requirements.txt
WAITING SOME LONG LONG TIME...

```
#### Exiting from the __matrix__
```bash
[pequenoponey@labhub ~/] $: CTRL+D

```

By, by __Neo__!

#### RELODING THE __MATRIX__
__Go to your flavor browser and type: http://172.17.0.X:8000. And if you do not
mess up anything you should get the login screen. Be useful and do some
science__

#### Future work

- [X] . Redirect the server (ipfilter);
- [ ] . Update this doc;
- [ ] . Change the network from bridge to host;
- [ ] . Deploy this service.
- [ ] . Stop the laziness ...
- [ ] . Stop the procastination...

#### License
---
***

Copyright 2021 NETLAB - ICE - UFJF

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---
#### Author
[Frederico Sales - Telegram](https://t.me/FredericoSalesTheThird)<br />
<frederico.sales@engenharia.ufjf.br><br />
[netlab](http://netlab.ice.ufjf.br)<br />
[ppgcc](https://www2.ufjf.br/pgcc/)<br />
[ufjf](https://ufjf.br)<br />
![netlab_ppgcc](http://netlab.ice.ufjf.br/~frederico/static/images/netlab_ppgcc.png)

---
[| up |](http://netlab.ice.ufjf.br/~frederico/util/jupyterhub.md) [back |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
<script>
	MathJax = {
		tex: {
			inlineMath: [['$', '$'], ['\\(', '\\)']]
		},

		svg: {
			fontCache: 'global'
		}
	};
</script>
