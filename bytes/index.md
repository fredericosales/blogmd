<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
# Mad Monkey Bytes
---
[| back |](http://netlab.ice.ufjf.br/~frederico)

#### Laminação de modelos com filme para laminação BOPP.
As bobinas para laminação a quente  garantem maior durabilidade e resistência
ao seu impresso. Depois de aplicada oferecem ótima proteção à umidade, maior
espessura e corpo para sua mídia impressa. O Bopp é ideal para impressão
offset. Utilizando o Bopp fosco o seu impresso apresenta um acabamento sedoso,
sem reflexos, minimiza marcas de digitais.<br />
[Leia mais aqui...](../util/laminacao)

#### Weapons of mass destruction...
A weapon of mass destruction (WMD) is a nuclear, radiological, chemical,
biological, or any other weapon that can kill and bring significant harm to
numerous humans or cause great damage to human-made structures (e.g.,
buildings), natural structures (e.g., mountains), or the biosphere. The scope
and usage of the term has evolved and been disputed, often signifying more
politically than technically. Originally coined in reference to aerial bombing
with chemical explosives during World War II, it has later come to refer to
large-scale weaponry of other technologies, such as chemical, biological,
radiological, or nuclear warfare.<br />
[Source: wickpedia.](https://en.wikipedia.org/wiki/Weapon_of_mass_destruction)

#### The United States and the others.
The USA government tells us history and nukes some people with white
phosphorus, dirty tank shells, and some more stuff ignoring the Genebra’s
treaty and ONU’s directives.  Besides, they are the world’s policy and perform
like so.

The American people act like there is no tomorrow. Only concerned with
consumption and produce the majority of the world’s waste and carbon gas.

Therebefore, all the other countries act like then. Do nothing about
environmental issues. The Brazilian people are a pour carbon copy of the USA. We
burn our forests, throw waste on water sources, and at last, we are at the
lowest level of Abraham Maslow’s pyramid.  The Brazilian government is a bunch
of clowns. The larger part does not have any moral ground and lies to conduct
the flok formed of functional illiterates to fight each other and make a
smokescreen to do the illicit acts by turning people against each other with
binary thoughts.

Each election is a horror show formed by the most bizarre candidates. Some have
anger issues and yellow like a mad dog. Between all these, they throw public
money into musical and artistic events to add some more to their flok. Some
build soccer stadiums others are bike riders. All consider good politic here in
Brazil knows how to do their task in the moment schedule.<br />
Author: [Frederico Sales](https://t.me/FredericoSalesTheThird)

#### How to make an easy, and simple nuke.

<pre>
   .------------------------------------------------------------------------.
   |   H O W   T O   M A K E   A   T H E R M O N U C L E A R   D E V I C E  |
   '------------------------------------------------------------------------'

If  you  enjoy  build, you'll really love this one - it will make you an
even bigger hit at parties.


1.    .--------.       Visit  your  local junk yard and get a lead box or if you
      |========|       can't find one, use an old coffee can.
      |        |
      | ------ |       Obtain  107kg of weapons grade Plutonium. This is readily
      | Coffee |       available from the Russian Mafia.
      | ------ |       Wash your hands after handling and don't  allow  children
      |        |       or pets to eat it). Store the Plutonium in the can.
      |________|

2.      ______
   ____|[____]|____
  |                |
  |                |
  |                |   You'll need a metal enclosure to house the device.
  |                |   Any  common  variety  of  sheet metal can be bent to the
  |                |   desired shape as can  a  metal  briefcase but DO NOT use
  |                |   aluminium foil.
  |                |
  |                |
  |________________|

3.
       .-:##:-.
      /  |##|  \      Arrange the Plutonium in two hemisphere held 3cm apart by
     |   |##|   |     rubber cement.
      \  |##|  /
       `-:##:-'
4.
      _.-""""-._
    .'          `.
   /   .-:##:-.   \   Pack the outside of the Plutonium with a thick  layer  of
  |   /  |##|  \   |  gelignite.  If  you  can't get gelignite you can used TNT
  |  |   |##|   |  |  packed in Playdo or any modeling clay. It  is  acceptable
  |   \  |##|  /   |  to  use  coloured  clay  but there's no need for anything
   \   `-:##:-'   /   fancy.
    `._        _.'
       `-....-'
5.
        ______
   ____|[____]|____
  |   _.-""""-._   |
  | .'          `. |
  |/   .-:##:-.   \|
  |   /  |##|  \   |  Fix  the  structure to the inside of the metal case using
  |  |   |##|   |  |  strong glue such as SupaGlue or similar. This must  be  a
  |   \  |##|  /   |  firm bond to prevent accidental detonation resulting from
  |\   `-:##:-'   /|  vibration or mishandling.
  | `._        _.' |
  |____`-....-'____|


6.      ______
   ____|[____]|____
  |                |  Attach an RC (radio controlled) servo  mechanism  as  used
  |                |  in  model  aircraft  and  cars to the case and fix a small
  |       __       |  plunger to the servo.
  |      /MM\      |
  |      \WW/      |  Remote detonation is achieved when the plunger  strikes  a
  |                |  detonator cap. These can be found in the electrical supply
  |                |  section  of  your  local supermarket. A good brand to look
  |                |  out for is "Blasto-O-Matic".
  |________________|


     You are now the pround owner of what used to be known as an "Atom Bomb".

  Theoretically the device works when the denotated TNT compresses the Plutonium
  into  a  crital  mass.  This  critical  mass produces a nuclear chain reaction
  similar to the domino effect (named after the disasterous domino  incident  in
  Hiroshima  in  1945  in  which  thousands of people died). The chain  reaction
  instantaneously produces a massive thermonuclear reaction commonly known as  a
  10 megaton explosion.

7. Unknown author (A.K.A hero).
</pre>
---
[| up |](http://netlab.ice.ufjf.br/~frederico/bytes/) [| back |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.
><script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
<script>
    MathJax = {
        tex: {
            inlineMath: [[&lsquo;$&rsquo;, &lsquo;$&rsquo;], [&lsquo;\(&rsquo;, &lsquo;\)&rsquo;]]
        },</p>

<code>    svg: {
        fontCache: 'global'
    }
};
</code>
</script>
