<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## Files
---
[| back |](http://netlab.ice.ufjf.br/~frederico/)


#### Arquivos

- **Bind conf files**
   * [download](bind.7z)

- **Create Virtual Machines - Virtualbox**
   * [download](create_vm.7z)

- **Modelo de dissertação PPGCC - 2019**
   * [download](dissetacao_ppgcc.7z)

- **Vim config file**
   * [download](vimrc.7z)

---
[| up |](http://netlab.ice.ufjf.br/~frederico/files/) [ back |](http://netlab.ice.ufjf.br/~frederico/)

<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script>
    MathJax = {
        tex: {
            inlineMath: [[&lsquo;$&rsquo;, &lsquo;$&rsquo;], [&lsquo;\(&rsquo;, &lsquo;\)&rsquo;]]
        },</p>

   svg: {
        fontCache: 'global'
    }
};
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
