<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## Utilidades
---
[ | Home](http://netlab.ice.ufjf.br/~frederico/)
[ | Bytes](http://netlab.ice.ufjf.br/~frederico/bytes/)
[ | Files](http://netlab.ice.ufjf.br/~frederico/files/)
[ | NetLab](http://netlab.ice.ufjf.br/)
[ | PPGCC](https://www2.ufjf.br/pgcc/)
[ | UFJF |](https://www2.ufjf.br/ufjf/)

---

    Frederico Sales <frederico.sales@engenharia.ufjf.br> - NetLab

#### [Servidor jupyterhub com jupyterlab como padrão](https://jupyterhub.readthedocs.io/en/stable/)

Utilizar o __jupyterhub__ é a melhor maneira para servir notebooks para
multiplos usuários. Ele pode ser utilizado por um
[laboratório](http://netlab.ice.ufjf.br/~frederico/util/jupyterhub.md), __grupo
de pesquisa__ ou até mesmo uma __empresa__. A solução cria instancias para
usuários que possuem conta fisica na maquina e têm total liberdade para instalar
e gerenciar seu ambiente. Link para a
[imagem](https://hub.docker.com/r/satangos/jupyterhub) docker.<br />[Leia aqui o
artigo completo - 20/06/2021.](https://jupyterhub.readthedocs.io/en/stable/)

#### [Dicas gitlab](http://netlab.ice.ufjf.br/~frederico/util/gitlab.md)
Gerenciamento de projetos, sem ferramentas complicadas. Com [gitlab](https://gitlab.com) você tem uma
plataforma aberta, confiável e sempre disponível para gerenciar projetos
simples ou com grande numero de colaboradores com segurança.<br />
[Leia aqui o artigo
completo - 15/06/2021.](http://netlab.ice.ufjf.br/~frederico/util/gitlab.md)

#### [Distorsões cognitivas](http://netlab.ice.ufjf.br/~frederico/util/distorcoes_cognitivas.md)
As __DISTORÇÕES COGNITIVAS__ são basicamente maneiras distorcidas de processar uma
informação, ou seja, são interpretações enviesadas do que nos acontece, criando
diversas consequências negativas.  Quando se sofre de depressão, por exemplo,
tem-se uma visão da realidade na qual as distorções cognitivas exercem um papel
principal __(visão de túnel)__. Todos nós podemos utilizar algum tipo de distorção
cognitiva. Saber detectá-las e analisá-las ajuda-nos a desenvolver atitudes
mais realistas e, acima de tudo, mais funcionais.<br />
[Leia aqui o artigo completo - 14/06/2021.](http://netlab.ice.ufjf.br/~frederico/util/distorcoes_cognitivas.md)

#### [Como as gerações se diferenciam](http://netlab.ice.ufjf.br/~frederico/util/geracoes.md)
É preciso fazer algumas considerações sobre essas divisões. Essas datas de
nascimento variam muito conforme as fontes e não há um consenso sobre elas. A
separação por anos também não é estanque: por exemplo, alguém que nasceu em
1990 pode ter um comportamento parecido de alguém nascido em 1980 em outro
lugar do país, em outra situação socioeconômica, em outro contexto.

Além disso, os nascidos no início de uma geração sofrem influência da geração
anterior, por isso alguns comportamentos se misturam. Por exemplo, quem nasceu
no início da década de 80 talvez tenha mais características da Geração X que
dos millennials.

Portanto, entenda que as datas de nascimento são aproximadas e que essas
separações geracionais são apenas tendências que identificam grande parte do
grupo, mas que não podem ser generalizadas para todos os nascidos nestes anos.
<br />
[Leia aqui o artigo
completo - 14/06/2021.](http://netlab.ice.ufjf.br/~frederico/util/geracoes.md)

---
[| up |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.
<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
<script>
    hljs.initHighlightingOnLoad();
</script>
<script>
    MathJax = {
        tex: {
            inlineMath: [['$', '$'], ['\\(', '\\)']]
        },

        svg: {
            fontCache: 'global'
        }
    };
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
