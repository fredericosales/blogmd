<link rel="stylesheet" type="text/css" media="all" href="http://netlab.ice.ufjf.br/~frederico/static/css/screen.css" />
## MCAST-VLC

#### Resumo
O protocolo **MCAST-VLC** foi elaborado com objetivo de utilizar as
consolidadas caracteristicas multicast para o espectro visivel para comunição.
O protocolo possui como principal atributo o controle e gerenciamento de grupos
pela manipulação de estados dos nśo membros da malba de forma a otimizar o
fluxo de rede com sobrecarga despresivel. De acordo com as caracteristicas da
malha ele possibilita a troca de protololo de roteamento de forma a melhor
atender pontualmente a condição da malha. 


#### Motivação


#### Proposta


#### Escopo

---
[| up |](http://netlab.ice.ufjf.br/~frederico/)

    All rights reserved to MadMonkeyBytes 2021.
<script src="http://netlab.ice.ufjf.br/~frederico/static/js/highlight.min.js" />
    <script>
        hljs.initHighlightingOnLoad();
    </script>
    <script>
        MathJax = {
            tex: {
                inlineMath: [['$', '$'], ['\\(', '\\)']]
            },
                                        
        svg: {
            fontCache: 'global'
        }
};
</script>
<script type="text/javascript" id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-svg.js" />
